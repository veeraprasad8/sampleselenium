package sample.sampleselenium;

import org.omg.CORBA.TIMEOUT;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCase1 {
	
	
	@Test(groups = "smoke")
	public void m1() {
		
		System.out.println("this is m1 test case");
		
	}
	
	@Test(groups = "smoke")
	public void m2() {
		
		System.out.println("this is m2 test case");
	}
	
	@Test(dependsOnGroups = "smoke")
	public void m3() {
		
		System.out.println("this is m3 test case");
	}
	


}
